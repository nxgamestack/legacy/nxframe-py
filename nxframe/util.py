def bytes_eq_at(octets: bytes, index, search: bytes):
    assert(len(search) == 1)
    try:
        return octets[index] == ord(search)
    except IndexError:
        pass
    return False


def find_byte_esc(octets: bytes, search: bytes, start_index=0, escaped=True):
    assert(len(search) == 1)
    i = start_index
    while i < len(octets):
        if bytes_eq_at(octets, i, search):
            if escaped:
                # check bounds so characters at end of bytes
                # aren't seen as unescaped by default
                if (i + 1 < len(octets)):
                    if bytes_eq_at(octets, i+1, search):
                        i = i + 1
                    else:
                        return i
                else:
                    return -1
            else:
                return i
        i = i + 1
    return -1

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


if __name__ == "__main__":
    assert(bytes_eq_at(b'cake', 0, b'c'))
    assert(bytes_eq_at(b'cake', 1, b'a'))
    assert(find_byte_esc(b'cheese', b'e') == -1)
    assert(find_byte_esc(b'cheese', b's') == 4)
