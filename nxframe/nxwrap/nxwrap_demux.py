from nxwrap.nxwrap_message import NxwrapMessage
from nxwrap.nxwrap import nxwrap_unwrap
from nxwrap.nxwrap import nxwrap_find_frame


class NxwrapDemultiplexer():

    def __init__(self):
        self._buffer = b''

    def push(self, octets:bytes):
        self._buffer += octets

    def peek(self):
        location = nxwrap_find_frame(self._buffer)
        source_id, kind, channel_id, data = nxwrap_unwrap(self._buffer, *location)
        return NxwrapMessage(source_id, kind, channel_id, data)

    def pop(self):
        location = nxwrap_find_frame(self._buffer)
        source_id, kind, channel_id, data = nxwrap_unwrap(self._buffer, *location)
        message = NxwrapMessage(source_id, kind, channel_id, data)
        if source_id:
            self._buffer = self._buffer[location[1]+1:]
            return message
        return None
